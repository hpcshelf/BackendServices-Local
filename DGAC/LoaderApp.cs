﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using org.hpcshelf.DGAC.utils;

namespace org.hpcshelf.DGAC
{
    public class LoaderApp
    {
		public static T deserialize<T>(string contents)
		{
			string filename = Path.GetTempFileName ();
            File.WriteAllText(filename, contents);

			// Declare an object variable of the type to be deserialized.
			T i = default(T);
			FileStream fs = null;
			try
			{
				Console.WriteLine("Reading with XmlReader " + filename);

				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(typeof(T));

				// A FileStream is needed to read the XML document.
				fs = new FileStream(filename, FileMode.Open);

				XmlReader reader = new XmlTextReader(fs);

				// Use the Deserialize method to restore the object's state.
				i = (T)serializer.Deserialize(reader);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
			finally
			{
				if (fs != null)
					fs.Close();
			}

			return i;

		}

		public static Instantiator.InstanceType DeserializeInstantiator(string filename)
        {
            // Declare an object variable of the type to be deserialized.
            Instantiator.InstanceType i = null;
            FileStream fs = null;
            try
            {
                Console.WriteLine("Reading with XmlReader");

                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.InstanceType));

                // A FileStream is needed to read the XML document.
                fs = new FileStream(filename, FileMode.Open);

                XmlReader reader = new XmlTextReader(fs);

                // Use the Deserialize method to restore the object's state.
                i = (Instantiator.InstanceType)serializer.Deserialize(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }

            return i;

        }


	/*	public static string serialize<T>(T instance)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			string filename = Path.GetTempFileName ();

			FileStream fs = new FileStream(filename, FileMode.Open);

			XmlWriter writer = new XmlTextWriter(fs, null);

			serializer.Serialize(writer, instance);

			fs.Close();

			return File.ReadAllText(filename);	

		}
*/
		public static byte[] serializeInstantiator(string filename, Instantiator.InstanceType inst)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.InstanceType));

            FileStream fs = new FileStream(filename, FileMode.Create);

            XmlWriter writer = new XmlTextWriter(fs, null);

            serializer.Serialize(writer, inst);

            BinaryReader br = new BinaryReader(fs);
            int count = (int)fs.Length;
            fs.Position = 0;

            byte[] data = br.ReadBytes(count);
            br.Close();
            fs.Close();

            return data;
        }


		public static string serialize<T>(T inst)
		{
			string filename = Path.GetTempFileName ();

			XmlSerializer serializer = new XmlSerializer(typeof(T));

			FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);

			XmlWriter writer = new XmlTextWriter(fs, null);

			serializer.Serialize(writer, inst);

			fs.Close();     

			string result = File.ReadAllText(filename);                 

			return result;
		}

		//receives id_concrete and id inner wich belongs for that inner
        //returns a impl of the inner
        //return -1 if the impl doesnt exist    




	}//class

}//namespace
