﻿using System;
using gov.cca;
using org.hpcshelf.DGAC;
using System.Collections.Generic;
using System.Threading;
using org.hpcshelf.ports;
using System.Collections.Concurrent;

namespace org.hpcshelf.DGAC
{
    public class MultipleReleasePortImpl : MultipleReleasePort
    {
        private IList<ReleasePort> ports = new List<ReleasePort>();

        public void addPort(ReleasePort port)
        {
            ports.Add(port);
        }

        public void release1()
        {
            foreach (ReleasePort port in ports)
                port.release1();
        }

        public void release2()
        {
            foreach (ReleasePort port in ports)
                port.release2();
        }
    }




    public class ReleasePortImpl : ReleasePort
	{
		private ManagerComponentID mcid = null;
		private string session_id_string = null;
		private Port[] w_release_port = null;
		
		public ReleasePortImpl(ManagerServices services, Port[] wrelease_ports)
		{
		   this.mcid = (ManagerComponentID) services.getComponentID();
		   this.session_id_string = mcid.getInstanceName();
		   this.w_release_port = wrelease_ports;
		}

		#region AutomaticSlicesPort implementation
		public void release1 ()
		{			
            IDictionary<Thread, Release1Thread> thread_list = new Dictionary<Thread,Release1Thread>();
			foreach (Port port in w_release_port)
            {
                Release1Thread thread = new Release1Thread((ReleasePort)port);
                Thread t = new Thread(thread.Run);
                thread_list.Add(t, thread);
                t.Start();
            }
            foreach (KeyValuePair<Thread,Release1Thread> t in thread_list)
            {
                try 
                {
					t.Key.Join();
                   Console.Error.WriteLine("Worker thread arrived RELEASE 1: " + session_id_string);
                } 
                catch (Exception e)
                {
                   Console.WriteLine("Worker failed RELEASE 1: " + session_id_string + ". error =" + e.Message);
                }
            }			
            Console.Error.WriteLine("Joined Threads RELEASE 1: " + session_id_string);
            
			
		}

        public void release2()
        {
            IDictionary<Thread, Release2Thread> thread_list = new Dictionary<Thread, Release2Thread>();
            foreach (Port port in w_release_port)
            {
                Release2Thread thread = new Release2Thread((ReleasePort)port);
                Thread t = new Thread(thread.Run);
                thread_list.Add(t, thread);
                t.Start();
            }
            foreach (KeyValuePair<Thread, Release2Thread> t in thread_list)
            {
                try
                {
                    t.Key.Join();
                    Console.Error.WriteLine("Worker thread arrived RELEASE 2: " + session_id_string);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Worker failed RELEASE 2: " + session_id_string + ". error =" + e.Message);
                }
            }
            Console.Error.WriteLine("Joined Threads RELEASE 2: " + session_id_string);


        }

        #endregion

        internal class Release1Thread
        {
			private ReleasePort worker_relese_port;

            public Release1Thread(ReleasePort worker_release_port)
            {
				this.worker_relese_port = worker_release_port;
            }

            public void Run()
            {
                try
                {
                    worker_relese_port.release1();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        internal class Release2Thread
        {
            private ReleasePort worker_relese_port;

            public Release2Thread(ReleasePort worker_release_port)
            {
                this.worker_relese_port = worker_release_port;
            }

            public void Run()
            {
                try
                {
                    worker_relese_port.release2();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

    }
}

