﻿// /home/jefferson/projetos/appexample/appexample/kinds/ApplicationKind.cs created with MonoDevelop
// User: jefferson at 13:21 29/5/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using org.hpcshelf.unit;
using gov.cca.ports;

namespace org.hpcshelf.kinds
{

	public interface ICertifierKind : IApplicationKind, IUnit
	{
	}

	//[Serializable]
	public abstract class Certifier : Application, ICertifierKind
	{
	}

}
