﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;
using org.hpcshelf.unit;
using System.ServiceModel;

namespace org.hpcshelf.ports
{
    [ServiceContract]
	[ServiceKnownType(typeof(InitializePortWrapper))]
    public interface InitializePort : Port
    {
		[OperationContract]
        void on_initialize();

		[OperationContract]
     	void after_initialize();
    }

	[ServiceContract]
	[ServiceKnownType(typeof(InitializePortWrapper))]
	public interface MultipleInitializePort : InitializePort
	{
        void addPort(InitializePort port);
	}


}
