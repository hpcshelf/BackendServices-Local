﻿namespace org.hpcshelf.platform
{
	public interface IPlatformServices 
	{
		int    getNumberOfNodes   ();
		int    getBaseBindingPort ();
		string deploy             (string config_contents, byte[] key_file_contents);
		void   setPlatformRef     (string arch_ref);
		string getPlatformRef     ();
		string instantiate        (string component_ref, string[] ctree, int facet_instance, string[] channel_id, int[] channel_facet_instance, string[] channel_facet_address);
		void   run                (string component_ref);
        void   release            (string component_ref);
        string getStatus          ();

		string allocateBindingAddress(string channel_id, int n);
	}
}
