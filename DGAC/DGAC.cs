﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.ServiceModel;
using System.Threading;
using org.hpcshelf.DGAC.utils;
using gov.cca;
using XMLComponentHierarchy;
using org.hpcshelf.ports;
using gov.cca.ports;
using System.Collections.Concurrent;
using org.hpcshelf.exception;

namespace org.hpcshelf
{

    namespace DGAC
    {

        public class BackEnd
        {
            public static WorkerObject worker_framework = null;

            public BackEnd()
            {
                Console.WriteLine("DGAC is up and running.");
            }


            #region FRAMEWORK_INSTANTIATION

            public static gov.cca.AbstractFramework getFrameworkInstance()
            {
                Console.WriteLine("getFrameworkInstance 1 !!!");
                //TcpChannel channel = new TcpChannel(Constants.MANAGER_PORT/*prop, client_provider*/);
                //ChannelServices.RegisterChannel(channel);

                //RemotingConfiguration.ApplicationName = Constants.MANAGER_SERVICE_NAME;
                ManagerObject obj = ManagerObject.SingleManagerObject;
                return (gov.cca.AbstractFramework)obj;
            }

            private static gov.cca.AbstractFramework framework_instance = null;
            private static IpcClientChannel ch_save = null;

            public static gov.cca.AbstractFramework getFrameworkInstance(out IpcClientChannel ch)
            {
                Console.WriteLine("getFrameworkInstance 2 !!!");

                if (framework_instance == null)
                {

                    IDictionary prop = new Hashtable();
                    prop["portName"] = Constants.MANAGER_PORT_NAME;

                    System.Runtime.Remoting.Channels.BinaryClientFormatterSinkProvider client_provider = new System.Runtime.Remoting.Channels.BinaryClientFormatterSinkProvider();

                    ch_save = ch = new IpcClientChannel(prop, client_provider);
                    ChannelServices.RegisterChannel(ch, false);
                    object[] activateAttribute = { new UrlAttribute("ipc://" + Constants.MANAGER_PORT_NAME) };
                    ManagerObject obj = (ManagerObject)Activator.CreateInstance(typeof(ManagerObject), null, activateAttribute);
                    framework_instance = (gov.cca.AbstractFramework)obj;
                }

                ch = ch_save;

                return framework_instance;
            }


            public static gov.cca.AbstractFramework getFrameworkWorkerInstance(string node, int port, int rank)
            {
                WorkerObject wo = null;

                string service_name = Constants.WORKER_SERVICE_NAME + "-" + rank + "-";
                string uri_str = "tcp://" + node + ":" + port + "/" + service_name;

                Console.WriteLine("URI = " + uri_str + ",  SERVICE NAME = " + service_name);

                // Register the TCP channel.
                //        ChannelServices.RegisterChannel(new TcpChannel(), false);

                // Create a url attribute object.
                UrlAttribute attribute = new UrlAttribute(uri_str);
                object[] activationAttributes = new object[] { attribute };

                // Register the client for the remote object.
                //RemotingConfiguration.RegisterActivatedClientType    (typeof(WorkerObject),  uri_str);

                wo = (WorkerObject)Activator.CreateInstance(typeof(WorkerObject), new object[] { }, activationAttributes);
                //wo = new WorkerObject();
                Console.WriteLine("wo is null ? " + wo == null);
                wo.sayHi();

                return wo;
            }

            public static gov.cca.AbstractFramework getFrameworkWorkerInstanceWCF(string node, int port, int rank)
            {
                string service_name = Constants.WORKER_SERVICE_NAME + "-" + rank;
                string uri_str = "http://" + node + ":" + port + "/" + service_name;

                Console.WriteLine("getFrameworkWorkerInstanceWCF - uri_str = " + uri_str);

                var binding = new BasicHttpBinding();
                var address = new EndpointAddress(uri_str);
                RemoteWorkerObject wo = new RemoteWorkerObject(binding, address);
                

                //MyWorkerObjectFactory wo = new MyWorkerObjectFactory (binding, address);

                return wo /*.createWorkerObject()*/;
            }


            public static void releaseManager(IpcClientChannel ch)
            {
                ChannelServices.UnregisterChannel(ch);
            }

            #endregion FRAMEWORK_INSTANTIATION

            public static string getSiteName()
            {
                return Constants.SITE_NAME;
            }

            #region DEPLOY


      //      private static string sendCompileCommandToWorker(string library_path, string moduleName, SourceDeployer deployer, DeployArguments.SourceContentsFile[] sourceContents, string[] refs, byte[] key_file_contents, string userName, string password, String curDir)
     //       {
     //           return deployer.compileClass(library_path, moduleName, sourceContents, refs, key_file_contents, userName, password, curDir);
    //        }




            #endregion DEPLOY






            #region RUN

            internal static IDictionary<string, ISession> open_sessions = new Dictionary<string, ISession>();

            public interface ISession
            {
                string SessionID { get; }
                gov.cca.Services Services { get; }
            }

            private class Session : ISession
            {
                private string session_id;
                private gov.cca.Services services;
                public Session(string session_id, gov.cca.Services services)
                {
                    this.session_id = session_id;
                    this.services = services;
                }
                public string SessionID { get { return session_id; } }
                public gov.cca.Services Services { get { return services; } }
            }

            public static ISession startSession(string session_id)
            {
                try
                {
                    TypeMapImpl properties = new TypeMapImpl();

                    gov.cca.AbstractFramework frw = getFrameworkInstance();
                    gov.cca.Services frwServices = frw.getServices(session_id, "Session", properties);

                    // Builder Service Port
                    frwServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, properties);

                    // GoPort Service Port
                    frwServices.registerUsesPort(Constants.GO_PORT_NAME, Constants.GO_PORT_TYPE, properties);

                    open_sessions[session_id] = new Session(session_id, frwServices);

                    return open_sessions[session_id];
                }
                catch (Exception e)
                {
                    Console.WriteLine("## Exception: " + e.Message);
                    Console.WriteLine("## Inner Exception: " + (e.InnerException == null ? "NULL" : e.InnerException.Message));
                    Console.WriteLine("## Trace: " + e.StackTrace);
                    throw e;
                }
            }



            public static string[] getPorts(string session_id, string instance_id)
            {
                gov.cca.Services frwServices = open_sessions[session_id].Services;

                gov.cca.ports.BuilderService bsPort = (gov.cca.ports.BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                ComponentID sid = frwServices.getComponentID();

                string[] uses_ports = bsPort.getUsedPortNames(sid);

                return uses_ports;
            }


            public static void finishSession(string session_id)
            {
                try
                {
                    gov.cca.Services frwServices = open_sessions[session_id].Services;

                    //frwServices.unregisterUsesPort(Constants.GO_PORT_NAME);
                    frwServices.unregisterUsesPort(Constants.BUILDER_SERVICE_PORT_NAME);

                    open_sessions.Remove(session_id);

                    Console.WriteLine("Finishing");
                }
                catch (Exception e)
                {
                    Console.WriteLine("## Exception: " + e.Message);
                    Console.WriteLine("## Inner Exception: " + (e.InnerException == null ? "NULL" : e.InnerException.Message));
                    Console.WriteLine("## Trace: " + e.StackTrace);
                    throw e;
                }
            }

            public static ICollection<string> getSessions()
            {
                return open_sessions.Keys;
            }







            public static int session_id = -1;

            #endregion RUN

            private static IList<string> binding_sequentials = new List<string>();

            internal static void unbindComponentInstance(ComponentID provider_id, gov.cca.ports.BuilderService bsPort)
            {
                unbindComponentInstance(null, provider_id, bsPort);
            }

            internal static void unbindComponentInstance(ComponentID user_id, ComponentID provider_id, gov.cca.ports.BuilderService bsPort)
            {
                string component_id = provider_id.getInstanceName();

                Console.WriteLine(" BEGIN unbindComponentInstance ... {0}", component_id);

                ConnectionID[] conn_list = bsPort.getConnectionIDs(new ComponentID[] { provider_id });
                foreach (ConnectionID conn in conn_list)
                    if (conn.getProvider() == provider_id && (user_id == null || conn.getUser() == user_id))
                    {
                        Console.WriteLine("begin disconnecting {0} from {1} -- {2}", conn.getUser(), conn.getProvider(), provider_id);
                        bsPort.disconnect(conn, int.MaxValue);
                        Console.WriteLine("end disconnecting {0} from {1} -- {2}", conn.getUser(), conn.getProvider(), provider_id);
                    }

                Console.WriteLine(" END unbindComponentInstance ... {0}", component_id);
            }

            internal static void releaseComponentInstance(ResolveComponentHierarchy ctree, Services frwServices, gov.cca.ports.BuilderService bsPort)
            {
                string component_id = ctree.ID;

                IList<ComponentID> s_cid_list = cid_dictionary[component_id];
                ComponentID s_cid = s_cid_list[0];

                string release_port_name = component_id + "_" + Constants.RELEASE_PORT_NAME /*+ "-" + i*/;

                // CONNECT THE RELEASE PORT OF THE APPLICATION TO THE SESSION DRIVER.
                gov.cca.ComponentID host_cid = frwServices.getComponentID();
                frwServices.registerUsesPort(release_port_name, Constants.RELEASE_PORT_TYPE, new TypeMapImpl());
                // gov.cca.ports.BuilderService bsPort = (gov.cca.ports.BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                ConnectionID conn = bsPort.connect(host_cid, release_port_name, s_cid, Constants.RELEASE_PORT_NAME);
                Console.WriteLine("BEFORE RELEASE PORT " + component_id + " --- " + frwServices.GetHashCode());
                ReleasePort release_port = (ReleasePort)frwServices.getPort(release_port_name);
                Console.WriteLine("AFTER RELEASE PORT " + component_id + " --- " + frwServices.GetHashCode());

                release_port.release1();

                // removeProvidesPorts do ctree ...
                // componente usuário (ctree) deve dar releasePort nas user ports ...

                foreach (KeyValuePair<string, IList<ResolveComponentHierarchy>> inner in ctree.InnerComponents)
                {
                    string inner_id = inner.Key;

                    Console.WriteLine("releaseComponentInstance - LOOP inner {0} in {1}", inner_id, ctree.ID);

                    ResolveComponentHierarchy ctree_inner_1 = inner.Value[0];

                    if (ctree.IsPrivate[inner_id])
                    {
                        unbindComponentInstance(ctree_inner_1.CID, bsPort);
                        releaseComponentInstance(ctree_inner_1, frwServices, bsPort);
                    }
                    else
                    {
                        unbindComponentInstance(ctree.CID, ctree_inner_1.CID, bsPort);
                    }
                }

                frwServices.releasePort(release_port_name);
                bsPort.disconnect(conn, int.MaxValue);
                release_port.release2();
                frwServices.unregisterUsesPort(release_port_name);

                Console.WriteLine("BEGIN destroy instance -- {0}", ctree.CID);
                bsPort.destroyInstance(ctree.CID, int.MaxValue);
                Console.WriteLine("END destroy instance -- {0}", ctree.CID);

                cid_dictionary.Remove(ctree.ID);
                if (delayed_host_bindings.ContainsKey(ctree.ID))
                    delayed_host_bindings.Remove(ctree.ID);
                if (ctree_cache.ContainsKey(ctree.ID))
                    ctree_cache.Remove(ctree.ID);
                if (delayed_set_host.ContainsKey(ctree.ID))
                    delayed_set_host.Remove(ctree.ID);
            }


            internal static ComponentID createComponentInstance(ResolveComponentHierarchy ctree,
                                                               string session_id_string,
                                                               int facet_instance,
                                                               string[] channel_id,
                                                               int[] channel_facet_instance,
                                                               string[] channel_facet_address,
                                                               gov.cca.ports.BuilderService bsPort)
            {
                int kind = ctree.Kind;



                TypeMapImpl properties = new TypeMapImpl();
                properties[Constants.PORT_NAME] = ctree.PortName; //TODO ... ctree.getPortName(***) ????
                properties[Constants.UNIT_MAPPING] = ctree.UnitMapping;
                properties[Constants.FACET_INSTANCE] = facet_instance;
                properties[Constants.IGNORE] = facet_instance >= ctree.FacetList.Length || ctree.FacetList[facet_instance].Count == 0;
                properties[Constants.COMPONENT_KIND] = kind;
                properties[Constants.CHANNEL_ID] = channel_id.Clone();
                properties[Constants.CHANNEL_FACET_INSTANCE] = channel_facet_instance.Clone();
                properties[Constants.CHANNEL_FACET_ADDRESS] = channel_facet_address.Clone();
                properties[Constants.COMPONENT_KIND] = kind;

                if (kind == Constants.KIND_BINDING)
                {
                    if (!binding_sequentials.Contains(ctree.ID))
                        binding_sequentials.Add(ctree.ID);
                    properties[Constants.BINDING_SEQUENTIAL] = binding_sequentials.IndexOf(ctree.ID);
                }

                int[] facet_topology = null;
                calculate_inner_facet_topology(ctree.UnitMapping, ref facet_topology);
                properties[Constants.FACET_TOPOLOGY] = facet_topology;


                Console.WriteLine("createComponentInstance - BEFORE createInstance {0} -- {1}/{2}/{3}", ctree.ID, channel_id.Length, channel_facet_instance.Length, channel_facet_address.Length);
                ctree.CID = (ManagerComponentID)bsPort.createInstance(ctree.ID, ctree.ComponentChoice, properties);
                Console.WriteLine("createComponentInstance - AFTER createInstance {0}", ctree.ID);

                if (!cid_dictionary.ContainsKey(ctree.ID))
                    cid_dictionary[ctree.ID] = new List<ComponentID>();
                else
                    Console.WriteLine("createComponentInstance ---- TWO IDs {0}", ctree.ID);
                cid_dictionary[ctree.ID].Add(ctree.CID);

                Console.WriteLine("cid_dictionary[{0}]={1}", ctree.ID, ctree.CID);

                foreach (KeyValuePair<string, IList<ResolveComponentHierarchy>> inner in ctree.InnerComponents)
                {
                    string inner_id = inner.Key;

                    Console.WriteLine("createComponentInstance - LOOP inner {0} in {1}", inner_id, ctree.ID);

                    ResolveComponentHierarchy ctree_inner_1 = inner.Value[0];
                    ComponentID inner_cid;
                    if (ctree.IsPrivate[inner_id])
                        inner_cid = createComponentInstance(ctree_inner_1, session_id_string, facet_instance, channel_id, channel_facet_instance, channel_facet_address, bsPort);
                    else
                        inner_cid = cid_dictionary[ctree_inner_1.ID][0]; 

                    bindComponentInstance(ctree_inner_1, bsPort);
                    Console.WriteLine("INSTANTIATED " + inner_cid);

                    //else
                    //{
                    //    Console.WriteLine("createComponentInstance - NOT PRIVATE {0} in {1}", inner_id, ctree.ID);
                    //}
                }

                if (delayed_host_bindings.ContainsKey(ctree.ID))
                {
                    foreach (Thread t in delayed_host_bindings[ctree.ID])
                    {
                        t.Start();
                        t.Join();
                    }
                    delayed_host_bindings.Remove(ctree.ID);
                }

                return ctree.CID;
            }

            private static IDictionary<string, IList<ComponentID>> cid_dictionary = new Dictionary<string, IList<ComponentID>>();

			private static IDictionary<string, IList<Thread>> delayed_host_bindings = new Dictionary<string, IList<Thread>>();

			internal static void bindComponentInstance(ResolveComponentHierarchy ctree, gov.cca.ports.BuilderService bsPort)
            {
                Console.WriteLine("bindComponentInstance 2 --- start ... {0} delay={1}", ctree.ID, ctree.InnerComponents.Count);
                Console.WriteLine("cid_dictionary[{0}] ? {1} {2}", ctree.ID, cid_dictionary.ContainsKey(ctree.ID), ctree.ComponentChoice);

                bindAsProvider(ctree, bsPort);
                //bindAsUser(ctree, bsPort);
            }

         /*  private static void bindAsUser(ResolveComponentHierarchy ctree, BuilderService bsPort)
            {
                ComponentID user_id = cid_dictionary[ctree.ID][0];
                foreach (KeyValuePair<string, IList<ResolveComponentHierarchy>> ctree_inner_list in ctree.InnerComponents)
                {
                    string id_inner = ctree_inner_list.Key;
                    if (!ctree.IsPrivate[id_inner])
                    {
                        ResolveComponentHierarchy ctree_inner = ctree_inner_list.Value[0];
                        // Tests whether the public component has been instantiated.
                        if (cid_dictionary.ContainsKey(ctree_inner.ID))
                        {
                            ComponentID provider_id = cid_dictionary[ctree_inner.ID][0];
                            bsPort.connect(user_id, ctree_inner.CID.getInstanceName(), provider_id, Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS);
                        }

                    }
                }
            }*/

            private static void bindAsProvider(ResolveComponentHierarchy ctree, gov.cca.ports.BuilderService bsPort)
            {
                ComponentID provider_id = cid_dictionary[ctree.ID][0];
                if (provider_id is ManagerComponentID)
                {
                    IList<Tuple<string, ResolveComponentHierarchy>> hosts = ctree.HostComponents; //ctree_inner.checkHosts();

                    Console.WriteLine("BEFORE ITERATION HOST --- {0}", hosts.Count);
                    foreach (Tuple<string, ResolveComponentHierarchy> ctree_host_pair in hosts)
                    {
                        ResolveComponentHierarchy ctree_host = ctree_host_pair.Item2;
                        Console.WriteLine("ITERATION HOST NEW --- {0} {1}", ctree_host.ID, cid_dictionary.ContainsKey(ctree_host.ID));
                        Thread t_bind_host = new Thread(() =>
                        {
                            ComponentID user_id = cid_dictionary[ctree_host.ID][0];
                            Console.WriteLine("ctree_host.Key = {0}", ctree_host_pair.Item1);
                            ConnectionID[] conn_list = bsPort.getConnectionIDs(new ComponentID[1] { provider_id });
                            bool connected = false;
                            foreach (ConnectionID conn in conn_list)
                            {
                                Console.Write("TESTING CONNECTION -- {0} (user) to {1} (provider) ? ", user_id.getInstanceName(), provider_id.getInstanceName());
                                if (conn.getUser().Equals(user_id))
                                {
                                    Console.WriteLine(" TRUE");
                                    connected = true;
                                }
                                else
                                    Console.WriteLine(" FALSE");
                            }
                            if (!connected)
                                bsPort.connect(user_id, ctree_host_pair.Item1, provider_id, Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS);
                        });

                        // THE HOST COMPONENT HAS BEEN INSTANTIATED ?
                        // TODO: there is a problem here. The host component only appear as a host when it is instantiated. 
                        //       So, the test will allways return "true".
                        if (cid_dictionary.ContainsKey(ctree_host.ID))
                        {
                            // IF YES, BIND !
                            t_bind_host.Start();
                            t_bind_host.Join();
                        }
                        else
                        {
                            // IF NOT, CREATE A SUSPENSION.
                            Console.WriteLine("HOST NOT FOUND {0} -- top level ?", ctree_host.ID);
                            if (!delayed_host_bindings.ContainsKey(ctree_host.ID))
                                delayed_host_bindings[ctree_host.ID] = new List<Thread>();

                            delayed_host_bindings[ctree_host.ID].Add(t_bind_host);
                        }
                    }
                    Console.WriteLine("AFTER ITERATION HOST --- #{0}", ctree.GetHashCode());
                    //if (ctree.IsPrivate[ctree_inner.getPortName(ctree.ID)])
                }
                else
                    Console.WriteLine("UNEXPECTED -- provider_id ({0}) is not ManagerComponentID -- bindComponentInstance2", ctree.ID);
            }

            private static IDictionary<string, Tuple<gov.cca.ports.GoPort, ConnectionID>> go_port_cache = new Dictionary<string, Tuple<gov.cca.ports.GoPort, ConnectionID>>();

            //    private static readonly object run_lock = new object();

           /* private static IList<string> is_running = new List<string>();

            public static bool isRunning (string cRef)
            {
                return is_running.Contains(cRef);
            }
            */
            public static void runSolutionComponent(gov.cca.Services frwServices, string cRef)
            {

                Tuple<GoPort, ConnectionID> go_port_and_conn = null;

                Console.WriteLine("runSolutionComponent {0} -- {1}", cRef, go_port_cache.ContainsKey(cRef));

                if (!go_port_cache.TryGetValue(cRef, out go_port_and_conn))
                {
                    if (!cid_dictionary.ContainsKey(cRef))
                        throw new UninstantiatedPlatformException(cRef);

                    IList<ComponentID> s_cid_list = cid_dictionary[cRef];
                    ComponentID s_cid = s_cid_list[0];

                    string go_port_name = cRef + "_" + Constants.GO_PORT_NAME /*+ "-" + i*/;
                    
                    // CONNECT THE GO PORT OF THE APPLICATION TO THE SESSION DRIVER.
                    ComponentID host_cid = frwServices.getComponentID();
                    frwServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl());
                    BuilderService bsPort = (BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                    ConnectionID conn = bsPort.connect(host_cid, go_port_name, s_cid, Constants.GO_PORT_NAME);
                    Console.WriteLine("BEFORE GO PORT " + cRef + " --- " + frwServices.GetHashCode());
                    GoPort go_port = (GoPort)frwServices.getPort(go_port_name);
                    Console.WriteLine("AFTER GO PORT " + cRef + " --- " + frwServices.GetHashCode());

                    go_port_and_conn = go_port_cache[cRef] = new Tuple<GoPort, ConnectionID>(go_port, conn);
                }

                go_port_and_conn.Item1.go();
            }

            public static string tryReleaseGoPort(gov.cca.Services frwServices, string cRef)
            {
                string go_port_name = null;
                Tuple<gov.cca.ports.GoPort, ConnectionID> go_port_and_conn = null;
                if (go_port_cache.TryGetValue(cRef, out go_port_and_conn))
                {
                    Console.WriteLine("BEGIN tryReleaseGoPort {0}", cRef);
                    go_port_name = cRef + "_" + Constants.GO_PORT_NAME /*+ "-" + i*/;
                    gov.cca.ports.BuilderService bsPort = (gov.cca.ports.BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                    frwServices.releasePort(go_port_name);
                    Console.WriteLine("END tryReleaseGoPort {0}", cRef);
                    go_port_cache.Remove(cRef);
                }
                return go_port_name;
            }




            public static void copy_unit_mapping_to_dictionary(Instantiator.UnitMappingType[] unit_mapping,
                                                                out IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict)
            {
                unit_mapping_dict = new Dictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>>(); // !!!!!!
                foreach (Instantiator.UnitMappingType unit_node in unit_mapping)
                {
                    //    Console.WriteLine ("copy_unit_mapping_to_dictionary - adding " + unit_node.unit_id);

                    string key = unit_node.unit_id;
                    IList<Tuple<int, int[], int, string, string, string[]>> list = null;
                    if (!unit_mapping_dict.TryGetValue(key, out list))
                    {
                        list = new List<Tuple<int, int[], int, string, string, string[]>>();
                        unit_mapping_dict.Add(key, list);
                    }

                    if (unit_node.assembly_string.Equals(""))
                        Console.WriteLine("copy_unit_mapping_to_dictionary -- EMPTY ASSEMBLY STRING -- {0} / {1}", unit_node.unit_id, unit_node.class_name);

                    list.Add(new Tuple<int, int[], int, string, string, string[]>(unit_node.facet_instance, unit_node.node, unit_node.facet, unit_node.assembly_string, unit_node.class_name, unit_node.port_name));

                }
            }

            public static void count_nodes(int facet_instance, Instantiator.UnitMappingType[] unit_mapping, out int count_nodes)
            {
                count_nodes = 0;
                foreach (Instantiator.UnitMappingType unit_node in unit_mapping)
                    if (unit_node.facet_instance == facet_instance)
                        count_nodes += unit_node.node.Length;
                    else if (unit_node.facet_instance < 0)
                        count_nodes++;
            }


            private static void calculate_inner_facet_topology(Instantiator.UnitMappingType[] unit_mapping_inner, ref int[] facet_topology)
            {
                IList<Tuple<int, int>> facet_topology_list = new List<Tuple<int, int>>();

                int max = 0;
                foreach (Instantiator.UnitMappingType um in unit_mapping_inner)
                    if (um.facet_instance >= 0)
                    {
                        Console.WriteLine("calculate_inner_facet_topology: (" + um.facet_instance + "," + um.facet + ")");
                        facet_topology_list.Add(new Tuple<int, int>(um.facet_instance, um.facet));
                        max = um.facet_instance > max ? um.facet_instance : max;
                    }

                facet_topology = new int[max + 1];
                for (int i = 0; i < facet_topology.Length; i++)
                    facet_topology[i] = -1;

                foreach (Tuple<int, int> p in facet_topology_list)
                    facet_topology[p.Item1] = p.Item2;

                for (int i = 0; i < facet_topology.Length; i++)
                    Console.WriteLine("facet_topology[" + i + "] = " + facet_topology[i]);
            }


            public interface BaseComponentHierarchy
            {
                string ID { get; }
                IDictionary<string, IList<ResolveComponentHierarchy>> InnerComponents { get; }

                void addInnerComponent(string inner_id, ResolveComponentHierarchy inner_component);
                void accept(BaseComponentHierarchyVisitor visitor);
            }

            public interface ResolveComponentHierarchy : BaseComponentHierarchy
            {
                string ComponentChoice { get; }
                ComponentID CID { get; set; }

                IList<Tuple<string, ResolveComponentHierarchy>> HostComponents { get; }
                IDictionary<string, bool> IsPrivate { get; }
                int Kind { get; set; }
                Instantiator.UnitMappingType[] UnitMapping { get; set; }
                IList<int>[] FacetList { get; set; }
                string PortName { get; set; }

                void addHostComponent(string id_inner_host, ResolveComponentHierarchy host_component);
                IList<Tuple<string, ResolveComponentHierarchy>> checkHosts();
                string getPortName(string ctree_host_id);
            }

            private class BaseComponentHierarchyImpl : BaseComponentHierarchy
            {
                protected string id;
                public string ID { get { return this.id; } set { this.id = value; } }

                private IDictionary<string, IList<ResolveComponentHierarchy>> inner_components = new Dictionary<string, IList<ResolveComponentHierarchy>>();
                public IDictionary<string, IList<ResolveComponentHierarchy>> InnerComponents => this.inner_components;

                public BaseComponentHierarchyImpl(string id)
                {
                    this.ID = id;
                }

                public void addInnerComponent(string inner_id, ResolveComponentHierarchy inner_component)
                {
                    if (!InnerComponents.ContainsKey(inner_id))
                        inner_components[inner_id] = new List<ResolveComponentHierarchy>();
                    inner_components[inner_id].Add(inner_component);
                }

                public void addInnerComponent(string inner_id, IList<ResolveComponentHierarchy> inner_component)
                {
                    IList<ResolveComponentHierarchy> l = new List<ResolveComponentHierarchy>(inner_component);
                    if (!InnerComponents.ContainsKey(inner_id))
                        inner_components[inner_id] = l;
                    else
                        foreach (ResolveComponentHierarchy e in l)
                            inner_components[inner_id].Add(e);
                }

                public void accept(BaseComponentHierarchyVisitor visitor)
                {
                    visitor.visit(this);
                }
            }

            private class ResolveComponentHierarchyImpl : BaseComponentHierarchyImpl, ResolveComponentHierarchy
            {
                private string component_choice;

                private IDictionary<string, bool> is_private = new Dictionary<string, bool>();

                public ResolveComponentHierarchyImpl(string id) : base(id)
                {
                }

                public string ComponentChoice { get { return this.component_choice; } set { this.component_choice = value; } }


                public void addInnerComponent(string inner_id, bool is_private, IList<ResolveComponentHierarchy> inner_component)
                {
                    base.addInnerComponent(inner_id, inner_component);
                    if (!this.IsPrivate.ContainsKey(inner_id))
                        this.is_private[inner_id] = is_private;
                }

                public void addInnerComponent(string inner_id, bool is_private, ResolveComponentHierarchy inner_component)
                {
                    base.addInnerComponent(id, inner_component);
                    if (!this.IsPrivate.ContainsKey(inner_id))
                        this.is_private[inner_id] = is_private;
                }

                private IDictionary<string, ResolveComponentHierarchy> host_components_by_id = new Dictionary<string, ResolveComponentHierarchy>();
                private IList<Tuple<string, ResolveComponentHierarchy>> host_components = new List<Tuple<string, ResolveComponentHierarchy>>();
                public IList<Tuple<string, ResolveComponentHierarchy>> HostComponents => this.host_components;

                public void addHostComponent(string id_inner_host, ResolveComponentHierarchy host_component)
                {
                    if (!host_components_by_id.ContainsKey(host_component.ID))
                    {
                        Console.WriteLine("ADD HOST {0} TO {1}", host_component.ID, this.ID);
                        host_components.Add(new Tuple<string, ResolveComponentHierarchy>(id_inner_host, host_component));
                        host_components_by_id.Add(host_component.ID, host_component);
                    }
                    else
                    {
                        Console.WriteLine("HOST ALREADY SET --- {0} {1}", host_component.ID, this.ID);
                    }
                }

                public string getPortName(string ctree_host_id)
                {
                    Console.WriteLine("getPortName {0} {1} {2} {3}", this.ID, ctree_host_id, host_components_by_id.ContainsKey(ctree_host_id), this.PortName);
                    foreach (Tuple<string, ResolveComponentHierarchy> r in this.HostComponents)
                        Console.WriteLine("getPortName --- {0} {1}", r.Item1, r.Item2.ID);

                    ResolveComponentHierarchy ctree_host = host_components_by_id[ctree_host_id];
                    foreach (Tuple<string, ResolveComponentHierarchy> t in host_components)
                        if (t.Item2.ID.Equals(ctree_host.ID))
                            return t.Item1;

                    return null;
                }

                public IDictionary<string, bool> IsPrivate => is_private;

                private int kind = -1;
                public int Kind { get { return kind; } set { kind = value; } }

                private Instantiator.UnitMappingType[] unit_mapping = null;
                public Instantiator.UnitMappingType[] UnitMapping { get { return unit_mapping; } set { unit_mapping = value; } }

                private IList<int>[] facet_list = null;
                public IList<int>[] FacetList { get { return facet_list; } set { facet_list = value; } }

                private ComponentID cid = null;
                public ComponentID CID { get { return cid; } set { cid = value; } }

                private string port_name = null;
                public string PortName { get { return port_name; } set { port_name = value; } }

                public void accept(ResolveComponentHierarchyVisitor visitor)
                {
                    visitor.visit(this);
                }

                public IList<Tuple<string, ResolveComponentHierarchy>> checkHosts()
                {
                    IList<Tuple<string, ResolveComponentHierarchy>> result_hosts_copy = new List<Tuple<string, ResolveComponentHierarchy>>(this.HostComponents);
                    IList<Tuple<string, ResolveComponentHierarchy>> result_hosts = this.HostComponents;

                    bool next = false;
					foreach (Tuple<string, ResolveComponentHierarchy> host_2 in result_hosts)
					{
						next = false;
						foreach (Tuple<string, ResolveComponentHierarchy> host_1 in result_hosts)
						{
							foreach (Tuple<string, ResolveComponentHierarchy> host_host_1 in host_1.Item2.HostComponents)
							{
								if (host_host_1.Item2 == host_2.Item2)
								{
									result_hosts_copy.Remove(host_2);
									next = true;
									break;
								}
							}
						}
						if (next)
							break;
					}

					return result_hosts_copy;
				}
            }

            private static IDictionary<string, IList<Thread>> delayed_set_host = new Dictionary<string, IList<Thread>>();

			private static IDictionary<string, ResolveComponentHierarchy> ctree_cache = new Dictionary<string, ResolveComponentHierarchy>();
			private static void addToCTreeCache(ResolveComponentHierarchy ctree)
            {
                ctree_cache[ctree.ID] = ctree;
                if (delayed_set_host.ContainsKey(ctree.ID))
                {
                    foreach (Thread t in delayed_set_host[ctree.ID])
                    {
                        t.Start();
                        t.Join();
                    }
                    delayed_set_host.Remove(ctree.ID);
                }
            }

            public interface BaseComponentHierarchyVisitor
            {
                object visit(BaseComponentHierarchy ctree);
            }

            public interface ResolveComponentHierarchyVisitor
            {
                object visit(ResolveComponentHierarchy ctree);
            }

            public static ResolveComponentHierarchy readComponentHierarchy(ComponentHierarchyType ctree_xml, string port_name)
            {
                Console.WriteLine("READ COMPONENT HIERARCHY {0} {1}", ctree_xml.id, port_name);

                IDictionary<string, ResolveComponentHierarchy> memory = new Dictionary<string, ResolveComponentHierarchy>();
                ResolveComponentHierarchyImpl ctree = new ResolveComponentHierarchyImpl(ctree_xml.id);
                Console.WriteLine("NEW ResolveHierarchyImpl {0}", ctree.GetHashCode());

                addToCTreeCache(ctree);

                readComponentHierarchyNode(ctree, ctree_xml);
                ctree.PortName = port_name;

                readComponentHierarchy1(ctree_xml, ctree);
                readComponentHierarchy2(ctree_xml, ctree);

                return ctree;
            }

            private static void readComponentHierarchyNode(ResolveComponentHierarchyImpl ctree, ComponentHierarchyType ctree_xml)
            {
                Console.Write("COMPONENT CHOICE 2 ? {0}/{1}", ctree_xml.component_choice_2 == null, ctree_xml.unit_2 == null);
                if (ctree_xml.component_choice_2 != null)
                    Console.WriteLine(" --- {0} / {1}", ctree_xml.component_choice, ctree_xml.component_choice_2);
                else
					Console.WriteLine(" *** {0}", ctree_xml.component_choice);

				ctree.ComponentChoice = ctree_xml.component_choice;
                Instantiator.UnitMappingType[] unit = ctree_xml.unit;
                ctree.Kind = Constants.kindInt[ctree_xml.kind];

                if (ctree_xml is PrivateInnerComponentType)
                    ctree.PortName = ((PrivateInnerComponentType)ctree_xml).id_inner;


                int i = 0;
                ctree.UnitMapping = new Instantiator.UnitMappingType[unit.Length];
                foreach (Instantiator.UnitMappingType unit_mapping_x in unit)
                {
                    //Console.WriteLine("CLASS NAME: {0}", unit_mapping_x.class_name);
                    ctree.UnitMapping[i++] = unit_mapping_x;
                }

                int maxf = 0;
                foreach (FacetType f in ctree_xml.facet_list)
                    maxf = f.facet_instance > maxf ? f.facet_instance : maxf;

                ctree.FacetList = new List<int>[maxf + 1];
                for (int j = 0; j <= maxf; j++)
                    ctree.FacetList[j] = new List<int>();

                foreach (FacetType f in ctree_xml.facet_list)
                {
                    if (f.facet != null)
                        foreach (int ff in f.facet)
                            ctree.FacetList[f.facet_instance].Add(ff);
                }

            }

            private static void readComponentHierarchy1(XMLComponentHierarchy.ComponentHierarchyType ctree_xml, ResolveComponentHierarchy ctree)
            {
                if (ctree_xml.private_inner_component != null)
                    foreach (XMLComponentHierarchy.PrivateInnerComponentType inner_ctree_xml in ctree_xml.private_inner_component)
                    {
                        ctree.IsPrivate[inner_ctree_xml.id_inner] = true;
                        ResolveComponentHierarchyImpl inner_ctree = new ResolveComponentHierarchyImpl(inner_ctree_xml.id);
                        Console.WriteLine("NEW ResolveHierarchyImpl {0}", inner_ctree.GetHashCode());
                        readComponentHierarchyNode(inner_ctree, inner_ctree_xml);

                        addToCTreeCache(inner_ctree);

                        readComponentHierarchy1(inner_ctree_xml, inner_ctree);

                        ctree.addInnerComponent(inner_ctree_xml.id_inner, inner_ctree);
                    }
            }

            private static void readComponentHierarchy2(XMLComponentHierarchy.ComponentHierarchyType ctree_xml, ResolveComponentHierarchy ctree)
            {
                Console.WriteLine("readComponentHierarchy2 - INNER {0}", ctree.ID);
                if (ctree_xml.private_inner_component != null)
                    foreach (XMLComponentHierarchy.PrivateInnerComponentType inner_ctree_xml in ctree_xml.private_inner_component)
                    {
                        ResolveComponentHierarchy inner_ctree = null;
                        Thread t = new Thread(new ThreadStart(() =>
                        {
                            inner_ctree = ctree_cache[inner_ctree_xml.id];
                            readComponentHierarchy2(inner_ctree_xml, inner_ctree);
                        }));
                        if (ctree_cache.ContainsKey(inner_ctree_xml.id))
                        {
                            t.Start();
                            t.Join();
                        }
                        else
                            addToDelayedSetHost(inner_ctree_xml.id, t);
                    }

                Console.WriteLine("readComponentHierarchy2 - PUBLIC {0}", ctree.ID);
                if (ctree_xml.public_inner_component != null)
                    foreach (PublicInnerComponentType s in ctree_xml.public_inner_component)
                    {
                        ctree.IsPrivate[s.id_inner] = false;

                        Console.WriteLine("readComponentHierarchy2 - 1.1 {0} {1}", s.id_inner, s.inner == null);
                        if (s.inner != null)
                            foreach (PublicInnerComponentIdType inner_host_id in s.inner)
                            {
                                Console.WriteLine("readComponentHierarchy2 - 1.2 {0}", inner_host_id.id);
                                Thread t = new Thread(new ThreadStart(() =>
                                {
                                    Console.WriteLine("readComponentHierarchy2 - 1 --- {0} {1}", inner_host_id.id, ctree_cache.ContainsKey(inner_host_id.id));
                                    ResolveComponentHierarchy host_inner = ctree_cache[inner_host_id.id];
                                    ctree.addInnerComponent(s.id_inner, host_inner);
                                }));
                                if (ctree_cache.ContainsKey(inner_host_id.id))
                                {
                                    Console.WriteLine("readComponentHierarchy2 - 1.3 {0}", inner_host_id.id);
                                    t.Start();
                                    t.Join();
                                    Console.WriteLine("readComponentHierarchy2 - 1.4 {0}", inner_host_id.id);
                                }
                                else
                                {
                                    Console.WriteLine("readComponentHierarchy2 - 1.5 {0}", inner_host_id.id);
                                    addToDelayedSetHost(inner_host_id.id, t);
                                    Console.WriteLine("readComponentHierarchy2 - 1.6 {0}", inner_host_id.id);
                                }
                            }
                    }

                Console.WriteLine("readComponentHierarchy2 - HOST {0}", ctree.ID);
                if (ctree_xml.host_component != null)
                {
                    foreach (StringListType host_id in ctree_xml.host_component)
                    {
                        string id_host = host_id.id[0]; ;
                        Thread t = new Thread(new ThreadStart(() =>
                        {
                            //foreach (string id in host_id.id)
                            //{
                            Console.WriteLine("readComponentHierarchy2 - 2 --- {0} {1}", id_host, ctree_cache.ContainsKey(id_host));
                            ResolveComponentHierarchy host_inner = ctree_cache[id_host];
                            ctree.addHostComponent(host_id.id_inner, host_inner);
                            //}
                        }));

                        if (ctree_cache.ContainsKey(id_host))
                        {
                            t.Start();
                            t.Join();
                        }
                        else
                            addToDelayedSetHost(id_host, t);
                    }
                }

                Console.WriteLine("ADDED {0} HOSTS TO {1} #{2}", ctree.HostComponents.Count, ctree.ID, ctree.GetHashCode());

            }

            private static void addToDelayedSetHost(string v, Thread t)
            {
                Console.WriteLine("addToDelayedSetHost {0}", v);
                IList<Thread> lt;
                if (!delayed_set_host.TryGetValue(v, out lt))
                {
                    lt = new List<Thread>();
                    delayed_set_host[v] = lt;
                }
                lt.Add(t);
            }
        } //BackEnd



        public class PortUsageManager
        {
            bool fetched = false;

            private IDictionary<string, int> portFetches = new Dictionary<string, int>();
            private IDictionary<string, int> portReleases = new Dictionary<string, int>();

            private IDictionary<string, bool> portFetched = new Dictionary<string, bool>();

            public bool isFetched(string portname)
            {
                //int count = 0;
                //return portFetches.TryGetValue(portname, out count) && count > 0;
                Console.WriteLine("isFetched {0} -- {1} -- {2} -- {3}", portname, portFetches.ContainsKey(portname) ? portFetches[portname] : 0, portReleases.ContainsKey(portname) ? portReleases[portname] : 0, portFetched.ContainsKey(portname) ? portFetched[portname] : false);
                return portFetched.ContainsKey(portname) && portFetched[portname];
            }

            public bool isReleased(string portname)
            {
                //int count = 0;
                //return portReleases.TryGetValue(portname, out count) && count > 0;
                Console.WriteLine("isReleased {0} -- {1} -- {2} -- {3}", portname, portFetches.ContainsKey(portname) ? portFetches[portname] : 0, portReleases.ContainsKey(portname) ? portReleases[portname] : 0, portFetched.ContainsKey(portname) ? portFetched[portname] : false);
                return !portFetched.ContainsKey(portname) || !portFetched[portname];
            }

            public void addPortFetch(string portName)
            {
                if (!portFetches.ContainsKey(portName))
                    portFetches[portName] = 0;

                if (!portFetched.ContainsKey(portName))
                    portFetched[portName] = false;

                portFetches[portName]++;
                portFetched[portName] = true;

                // int count_fetch = 0;
                // if (portFetches.TryGetValue(portName, out count_fetch))
                //    portFetches.Remove(portName);

                // int count_release = 0;
                // if (portReleases.TryGetValue(portName, out count_release))
                //    portReleases.Remove(portName);

                // if (count_release > 0)
                // {
                //    Console.Error.WriteLine("Port " + portName + " was released (release count=" + count_release + ")");
                //    throw new CCAExceptionImpl(CCAExceptionType.PortNotInUse);
                // }

                // portFetches.Add(portName, count_fetch + 1);
            }

            public void addPortRelease(string portName)
            {
                if (!portReleases.ContainsKey(portName))
                    portReleases[portName] = 0;

                if (!portFetched.ContainsKey(portName))
                    portFetched[portName] = false;

                portReleases[portName]++;
                portFetched[portName] = false;

                /*int count_release = 0;
                if (portReleases.TryGetValue(portName, out count_release))
                    portReleases.Remove(portName);

                if (count_release > 0)
                {
                    Console.Error.WriteLine("Port " + portName + " still in use (release count = " + count_release + ")");
                    throw new CCAExceptionImpl(CCAExceptionType.UsesPortNotReleased);
                }

                portReleases.Add(portName, count_release + 1);
                */               
            }

            public void resetPort(string portName)
            {
                if (portReleases.ContainsKey(portName)) 
                    portReleases.Remove(portName);
                if (portFetches.ContainsKey(portName)) 
                    portFetches.Remove(portName);
                if (portFetched.ContainsKey(portName)) 
                    portFetched[portName] = false;
            }
        }
    }//namespac
}
