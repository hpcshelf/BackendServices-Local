#!/bin/sh

MAKE=make_worker.rsp
OLD="MONO_HOME"
NEW=$MONO_HOME

cp $MAKE temp.rsp
find ./ -iname temp.rsp | xargs sed -i --expression "s@$OLD@$NEW@"

if [ ! -d ./bin ]
then
        mkdir bin
fi

mcs @temp.rsp
rm temp.rsp

