#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include "hpcshelf.h"

int ___rank = -1;
MPI_Comm parent_comm;

int HPCShelf_Send(int key,
	          const void *buf,
	          int count,
	          MPI_Datatype datatype,
	          int facet,
	          int target,
	          int tag)
{
    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }

    int p[6] = { count, facet, target, datatype, tag, key };
    MPI_Send(p, 6, MPI_INT, ___rank, 333, parent_comm);    
    
    MPI_Send(buf, count, datatype, ___rank, 444, parent_comm);
    
    return 0;
}       
	   

int HPCShelf_Recv(int key,
	          void *buf,
	          int count,
	 	  MPI_Datatype datatype,
		  int facet,
		  int source,
		  int tag)
{
    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }
    
    int p[6] = { count, facet, source, datatype, tag, key };
    MPI_Send(p, 6, MPI_INT, ___rank, 555, parent_comm);    

    MPI_Status status;
    MPI_Recv(buf, count, datatype, ___rank, 666, parent_comm, &status);
    
    return 0;
}

int HPCShelf_Browse(const char *message)
{
    printf("%s", message); 

    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }
    
    char ranked_message[6 + strlen(message) + 1];
    sprintf(ranked_message, "%d: ", ___rank);
    strcat(ranked_message, message);

    int message_size = strlen(ranked_message);

    MPI_Send(&message_size, 1, MPI_INT, ___rank, 111, parent_comm);
    MPI_Send(ranked_message, message_size + 1, MPI_CHAR, ___rank, 222, parent_comm);
    
    return 0;
}

#define REQUEST_FACET 0 
#define REQUEST_FACET_COUNT 1
#define REQUEST_FACET_SIZE 2
#define REQUEST_FACET_INSTANCE 3

int HPCShelf_Get_facet(int key, int* facet)
{
    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }

    int v[2] = { REQUEST_FACET, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;    
    MPI_Recv(facet, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    
    return 0;
}

int HPCShelf_Get_facet_count(int key, int* facet_count)
{
    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }

    int v[2] = { REQUEST_FACET_COUNT, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;    
    MPI_Recv(facet_count, 1, MPI_INT, ___rank, 999, parent_comm, &status);
    
    return 0;
}       

int HPCShelf_Get_facet_size(int key, int* facet_size)
{
    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }

    int v[2] = { REQUEST_FACET_SIZE, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;
    
    int count;
    MPI_Recv(&count, 1, MPI_INT, ___rank, 888, parent_comm, &status);
    MPI_Recv(facet_size, count, MPI_INT, ___rank, 999, parent_comm, &status);
    
    return 0;
}       

int HPCShelf_Get_facet_instance(int key, int* facet_instance)
{
    if (___rank < 0)
    {
      MPI_Comm_get_parent(&parent_comm);
      MPI_Comm_rank(parent_comm, &___rank);
      printf("taking rank (%d) and parent_communicator (%d)", ___rank, parent_comm);
    }
    
    int v[2] = { REQUEST_FACET_INSTANCE, key };
    MPI_Send(v, 2, MPI_INT, ___rank, 777, parent_comm);

    MPI_Status status;
    
    int count;
    MPI_Recv(&count, 1, MPI_INT, ___rank, 888, parent_comm, &status);
    MPI_Recv(facet_instance, count, MPI_INT, ___rank, 999, parent_comm, &status);
    
    return 0;
}       

