#!/bin/sh
export HPE_NEXT_FREE_WORKER_PORT=4865
export HPE_NEXT_FREE_PLATFORM_SERVICE_PORT=8082
export HPE_PLATFORM_ACCESS_LOCAL=127.0.0.1
export MPIRUN_PATH=$HOME/backend_run
export HPE_PATH_PLATFORM_SERVICE=$HPCShelf_HOME/BackendServices/VirtualPlatformServices
export PATH_GRAPH_FILE=$HPCShelf_HOME/Case\ Study\ -\ Gust/carga_graph/amazon0302.e
sudo ps aux | grep -ie mono-service | awk '{print "kill -9 " $2}' | sudo sh -x
sudo ps aux | grep -ie mpirun | awk '{print "kill -9 " $2}' | sudo sh -x
sudo ps aux | grep -ie xsp | awk '{print "kill -9 " $2}' | sudo sh -x
rm $MPIRUN_PATH/Worker*.lock
$MONO_HOME/bin/xsp4 --port 8081
