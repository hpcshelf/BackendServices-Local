#!/bin/sh
export HPE_NEXT_FREE_WORKER_PORT=4865
export HPE_NEXT_FREE_PLATFORM_SERVICE_PORT=8082
export HPE_PLATFORM_ACCESS=127.0.0.1
export HPE_PLATFORM_ACCESS_LOCAL=127.0.0.1
export HPE_PATH_WORKER=/home/ubuntu/backendservices
export HPE_PATH_PLATFORM_SERVICE=/home/ubuntu/backendservices/VirtualPlatformServices
sudo ps aux | grep -ie mono-service | awk '{print "kill -9 " $2}' | sudo sh -x
sudo ps aux | grep -ie mpirun | awk '{print "kill -9 " $2}' | sudo sh -x
sudo ps aux | grep -ie xsp4 | awk '{print "kill -9 " $2}' | sudo sh -x
rm $HPE_PATH_WORKER/Worker*.lock
$MONO_HOME/bin/xsp4 --port 8080
