#include "mpi.h"

int HPCShelf_Send(int key,
	          const void *buf,
	          int count,
	          MPI_Datatype datatype,
	          int facet,
	          int target,
	          int tag);

int HPCShelf_Recv(int key,
	          void *buf,
	          int count,
	 	  MPI_Datatype datatype,
		  int facet,
		  int source,
		  int tag);

int HPCShelf_Browse(const char* message);

int HPCShelf_Get_facet(int key, int* facet);
int HPCShelf_Get_facet_count(int key, int* facet_count);
int HPCShelf_Get_facet_size(int key, int* facet_size);
int HPCShelf_Get_facet_instance(int key, int* facet_instance);
